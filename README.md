# Polly

#### Authors

- [Sandra Kuzmić](https://hr.linkedin.com/in/sandra-kuzmi%C4%87-53667610b) 
- [Lora Žuliček](https://hr.linkedin.com/in/lora-zulicek)
- [Blaž Bagić](https://hr.linkedin.com/in/bla%C5%BE-bagi%C4%87-555797138)
- [Matija Bartolac](https://hr.linkedin.com/in/matija-bartolac-805291a7)


## Digital Education project

Polly is an application created as a part of Digital Education course at FER, Zagreb. Main purpose of application is to allow professors to publish topics and student assignments and to allow students to "bid" for these topics. 

## Components

Project consists of three parts:
 - Authorization server
 - Server side application
 - Client web application

Authorizations server is constructed as OAuth2 authorization server to allow easier integration with other identity providers such as Google or Facebook. Server side applications is RESTful microservice that provides core functionality and communicates with data store. Client side application is single page application and it is fully rendered in browser

### Technologies

Server side applications are built using Spring framework. Authorization server is written in Kotlin and API is written using Java. Client side is built using React framework. Applications use embedded H2 database for storing data, but that can be extended to any relational database supported by Spring data JPA.

# Development
## Authorization server

Authorization server is standalone component and does not require other services in order to run. Only prerequisite is to have 'temp' folder in your home folder to store H2 database files, but that location can be changed editing `/auth-server/src/main/resources/application.yaml`. Application uses Liquibase for database migration. When running in development mode mock users and OAuth client are added. Provided user has username `admin` and password `admin` and assigned administrator role.

You are provided with Gradle wrapper so you don't need any additional tool for building source. Repository doesn't contain any IDE specific files or directories and you are free to use any that you find most comfortable to work with.

## API server

Application server depends on authorization server for fetching user data so you first need to start it. Read last section for instructions. Once authorization server is started there are no other modifications that you need to do to start application server.

You are provided with Maven wrapper so you don't need any additional tool for building source. Repository doesn't contain any IDE specific files or directories and you are free to use any that you find most comfortable to work with.

## Client application

In order to build and start client you need Npm and Node.js. If you just checked out project you need to run `npm install` to fetch all modules defined in `package.json`. After that, development server is started running comand `npm start`. If you are developing only client module running backend services is simplified with docker-compose. Just run from root project directory command ```docker-compose -f support/docker-compose.yaml up -d --build```. You can stop started services by running command ```docker-compose -f support/docker-compose.yaml down```

## Doucmentation

API documentation is genereated and can be found at `/app/swagger-ui.html`.

Each service is documented in code using JavaDoc.

# Usage
Main application is available at `/app`.
Admin interface is available at ` /auth/admin/users`

The main application allows managing polls and topics.
The admin interface is used to manage users, to add new users and edit or delete existing ones.

When starting the application for the first time You will be asked for OAuth approval. In order fo application to work select **Approve**.

## Features
### Creating a poll
Users with `ROLE_MODERATOR`  can create new polls. To create a new poll, simply fill all the needed information in Create Poll Modal.

### Grant poll access to other users
Users with `ROLE_MODERATOR`  can grant access to the polls they created. To grant access to other users, open Your poll and click on the button for assigning users to poll. Modal opens and there You can select users that You would like to grant access to.
When a user has access to the poll, it will automatically be shown on their home page.

### Topic import via CSV
Poll creator can simply add multiple topics using CSV import. CSV file is of following structure: `title, description, application_start, application_end, min_num_of_students, max_num_of_students, GROUP/PARALLEL, should_allow_students_unregister`.

Application start and end must be in format `yyyy-MM-dd`.

Sample CSV import can be found in project folder at `support/sample_import_topic.csv`.

### Poll and topics options
Title and description are mandatory fields.
The minimal and maximal number of students can be the same, or the maximum number must be greater than the minimum. 
`GROUP` option means that students work together on the same topic.
`PARALLEL` option means that students work on the same topic but individually.

### Adding new users
Users can be added manually or imported using CSV import.

Sample CSV import can be found in project folder at `support/sample_import_users.csv`.


### User roles
Application has three roles:
- `ROLE_ADMIN`
- `ROLE_MODERATOR`
- `ROLE_USER`.

Each user can have one or more roles.

`ROLE_ADMIN` matches the system's administrator who is in charge of adding users to the system and overall system maintenance. Administrator is also in charge of assigning users to moderators through application interface. There should be only one user of this type. 

`ROLE_MODERATOR`  is someone who can create polls and topics, and grant access to created polls to desired users. Each Moderator has associated users. These users are added by the Administrator, but can be associated with multiple Moderators. This is usually a teacher's role.

`ROLE_USER` is a user who has access to view polls and topics and assigns themselves for desired topics. This is usually the student's role.

|   | ROLE_ADMIN   | ROLE_MODERATOR | ROLE_USER  |
| :------------: | :------------: | :------------: | :------------: |
| Add new users   | X   |   |   |
| Delete users  |  X |   |   |
| Has only associated users  |  |  X  |   |
| Remove associated users  |  |  X  |   |
|  Create polls  |   |  X  |   |
|  Grant user access to poll  |   |  X  |   |
|  Create topics  |   | X  |  If poll options allows it |
| Assign for topic | | | X |
