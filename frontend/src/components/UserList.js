import React from "react";
import { Paper, Typography } from "@material-ui/core";

const UserItem = ({ id, fullName, isActive, handleClick }) => (
  <button
    type="button"
    className={
      isActive
        ? "list-group-item list-group-item-action active"
        : "list-group-item list-group-item-action"
    }
    onClick={() => handleClick(id)}
  >
    {fullName}
  </button>
);

export default function UserList({ title, users, activeUser, setActiveUser }) {
  const handleClick = id => setActiveUser(id);

  const renderUserItems = () => {
    return users.map(user => (
      <UserItem
        isActive={user.id === activeUser}
        fullName={user.name}
        id={user.id}
        key={user.id}
        handleClick={handleClick}
      />
    ));
  };

  return (
    <Paper elevation={2} style={{ backgroundColor: "lightgray" }}>
      <Typography variant="h5" style={{ textAlign: "center" }}>
        {title}
      </Typography>
        {users.length > 0 ? (
            <ul className="list-group">{renderUserItems()}</ul>
        ): (
            <Typography style={{ textAlign: "center" }}>
                Trenutno nema moderatora u sustavu
            </Typography>
        )}
    </Paper>
  );
}
