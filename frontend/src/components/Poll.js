import React from "react";
import { Link } from "react-router-dom";

import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import { COLORS } from "constants/colors";

const useStyles = makeStyles({
  poll: {
    backgroundColor: COLORS.white,
    boxShadow: "0 3px 5px 2px rgba(0, 0, 0, .12)",
    borderRadius: 8,
    padding: 24,
    marginBottom: 40,
    cursor: "pointer"
  },
  button: {
    alignSelf: "flex-end",
    fontSize: 12
  },
  buttonContainer: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-end"
  },
  title: {
    fontWeight: 700,
    fontSize: 24,
    marginBottom: 16
  },
  description: { marginBottom: 15, marginTop: 15 },
  pollText: {
    fontSize: 14,
    marginBottom: 8,
    position: "relative",
    color: COLORS.gray_5
  }
});

export default function Poll({
  id,
  title,
  description,
  topicCount,
  filledTopicCount,
  studentCount,
  maxStudentCount,
  deadline
}) {
  const classes = useStyles();

  return (
    <Box className={classes.poll}>
      <Link
        to={{
          pathname:"/app/projekt/" + id,
          pollId: id
        }}
        style={{ textDecoration: "none", color: "unset" }}
      >
        <Typography variant="h5" className={classes.title}>
          {title}
        </Typography>
        <Typography className={classes.description}>{description}</Typography>
        <Typography className={classes.pollText}>
          Broj popunjenih tema: {filledTopicCount}/{topicCount}
        </Typography>
        <Typography className={classes.pollText}>
          Broj prijavljenih studenata: {studentCount}/{maxStudentCount}
        </Typography>
        <Typography className={classes.pollText}>
          Rok prijave: {new Date(deadline).toLocaleDateString()}
        </Typography>

        <div className={classes.buttonContainer}>
          <Button className={classes.button}>Prikaži više ></Button>
        </div>
      </Link>
    </Box>
  );
}
