import React from "react";
import {makeStyles} from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import {Button,} from "@material-ui/core";
import {COLORS} from "constants/colors";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import {request} from "../../../util/APIUtils";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary
  },
  row: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center"
  },
  button: {
    backgroundColor: COLORS.primaryGreen,
    "&:hover": {
      backgroundColor: COLORS.primaryGreen,
      opacity: 0.8
    }
  },
  inputLabel: {
    "&:after": {
      borderBottom: "transparent"
    }
  },
  selectLabel: {
    display: "inline-block",
    marginRight: 8
  },
  select: {
    minWidth: 48,
    "&:after": {
      borderBottom: "transparent"
    }
  },
  error: {
    color: COLORS.primaryRed,
    marginTop: 8
  },
  disabled: {
    color: COLORS.gray_6
  },
  uploadLabel: {
    height: 40,
    border: "1px solid #28a745",
    color: COLORS.primaryGreen,
    padding: "0 16px",
    fontSize: 14,
    minWidth: 64,
    fontWeight: 500,
    lineHeight: "40px",
    borderRadius: 4,
    letterSpacing: 0.02857,
    textTransform: "uppercase",
    "&:hover": {
      cursor: "pointer"
    }
  },
  fileInput: {
    width: 0.1,
    height: 0.1,
    opacity: 0,
    overflow: "hidden",
    position: "absolute",
    zIndex: -1,
  },
  selectedFile: {
    fontSize: 16,
    lineHeight: "40px",
    padding: "0 8px",
  },
  removeButton: {
    marginLeft: 8,
    padding: 5,
    "&:hover": {
      cursor: "pointer"
    }
  }
}));

export const assignUserToPoll = (userId, pollId) => {
  return request({
    url: `/app/api/v2/poll/${pollId}/assign/${userId}`,
    method: "PUT"
  });
};

export default function AssignUser(props) {
  const classes = useStyles();
  const [selectedUser, setSelectedUser] = React.useState(null);

  console.log("kaboom");
  console.log(props.users.users);

  const handleSubmit = event => {
    event.preventDefault();
    assignUserToPoll(selectedUser, props.pollId)
      .then(() => {
        window.location.reload();
      })
  };

  const handleChange = (event) => {
    setSelectedUser(event.target.value);
  };


  return (
    <div className={classes.root}>
      <Grid container spacing={3}>
        <Grid item xs={10} style={{paddingTop: 20}}>
          <Select onChange={handleChange} value={selectedUser} style={{width: "200px"}}>
            {props.users.users.map(user =>
              <MenuItem key={user.id} value={user.id}>{user.name}</MenuItem>
            )}

          </Select>
        </Grid>
        <Grid item xs={3} style={{paddingTop: 20}}>
          <Button
            variant="contained"
            color="primary"
            className={classes.button}
            onClick={handleSubmit}
          >
            Dodaj
          </Button>
        </Grid>
      </Grid>
    </div>
  );
}
