import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import { Button } from "@material-ui/core";
import DateFnsUtils from "@date-io/date-fns";
import { COLORS } from "constants/colors";
import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import { importCSV } from "../../../util/APIUtils";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary
  },
  row: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center"
  },
  button: {
    backgroundColor: COLORS.primaryGreen,
    "&:hover": {
      backgroundColor: COLORS.primaryGreen,
      opacity: 0.8
    }
  },
  inputLabel: {
    "&:after": {
      borderBottom: "transparent"
    }
  },
  selectLabel: {
    display: "inline-block",
    marginRight: 8
  },
  select: {
    minWidth: 48,
    "&:after": {
      borderBottom: "transparent"
    }
  },
  error: {
    color: COLORS.primaryRed,
    marginTop: 8
  },
  disabled: {
    color: COLORS.gray_6
  },
  uploadLabel: {
    height: 40,
    border: "1px solid #28a745",
    color: COLORS.primaryGreen,
    padding: "0 16px",
    fontSize: 14,
    minWidth: 64,
    fontWeight: 500,
    lineHeight: "40px",
    borderRadius: 4,
    letterSpacing: 0.02857,
    textTransform: "uppercase",
    "&:hover": {
      cursor: "pointer"
    }
  },
  fileInput: {
    width: 0.1,
    height: 0.1,
    opacity: 0,
    overflow: "hidden",
    position: "absolute",
    zIndex: -1
  },
  selectedFile: {
    fontSize: 16,
    lineHeight: "40px",
    padding: "0 8px"
  },
  removeButton: {
    marginLeft: 8,
    padding: 5,
    "&:hover": {
      cursor: "pointer"
    }
  }
}));

export default function AddCSVForm(props) {
  const classes = useStyles();
  const [selectedFile, setSselectedFile] = React.useState(null);
  const [validationMsg, setValidationMsg] = React.useState("");

  const handleFileSubmit = event => {
    setSselectedFile(event.target.files[0]);
  };

  const handleFileDelete = () => {
    setSselectedFile(null);
  };

  const handleSubmit = event => {
    event.preventDefault();

    let data = new FormData();
    const csvdata = selectedFile;
    data.append("csv", csvdata);
    data.append("pollId", String(props.userId));
    data.append("userId", String(props.pollId));

    importCSV(data)
      .then(() => {
        props.closeModal();
        window.location.reload();
      })
      .catch(() => {
        setValidationMsg("Učtana datoteka nije ispravnog formata");
      });
  };

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <div className={classes.root}>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <input
              type="file"
              accept=".csv"
              name="file"
              className={classes.fileInput}
              onChange={handleFileSubmit}
              id="file"
            />
            <label htmlFor="file" className={classes.uploadLabel}>
              + Učitaj datoteku
            </label>
            {selectedFile && (
              <label className={classes.selectedFile}>
                {selectedFile.name}
                <span
                  onClick={handleFileDelete}
                  role="button"
                  className={classes.removeButton}
                >
                  X
                </span>
              </label>
            )}
            <span className={classes.error}>{validationMsg}</span>
          </Grid>
          <Grid item xs={3} style={{ paddingTop: 20 }}>
            <Button
              variant="contained"
              color="primary"
              className={classes.button}
              onClick={handleSubmit}
            >
              Spremi
            </Button>
          </Grid>
        </Grid>
      </div>
    </MuiPickersUtilsProvider>
  );
}
