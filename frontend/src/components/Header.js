import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import "./Header.scss";

class Header extends Component {
  isAdmin() {
    if (typeof this.props.user.roles !== "undefined") {
      return this.props.user.roles.indexOf("ROLE_ADMIN") !== -1;
    }
    return false;
  }

  render() {
    if (!this.props.authenticated) return null;

    return (
      <header>
        <div className="container">
          <nav className="app-nav">
            <ul>
              <li>
                <NavLink to="/app">Projekti</NavLink>
              </li>
              {!this.isAdmin() && this.props.user.roles.indexOf("ROLE_MODERATOR") === -1 && (
                <li>
                  <NavLink to="/app/moje_teme">Moje teme</NavLink>
                </li>
              )}
              {this.isAdmin() && (
                <li>
                  <NavLink to="/app/admin/assign">
                    Pridijeljivanje korisnika
                  </NavLink>
                </li>
              )}
              <ul>
                <li>
                  <NavLink to="/app/profile">Moj profil</NavLink>
                </li>
                <li>
                  <NavLink to={"#"} onClick={this.props.onLogout}>
                    Odjava
                  </NavLink>
                </li>
              </ul>
            </ul>
          </nav>
        </div>
      </header>
    );
  }
}

export default Header;
