import React from "react";
import { Route } from "react-router-dom";
import { config } from "../constants";

const PrivateRoute = ({ component: Component, authenticated, ...rest }) => {
  if (!authenticated) {
    window.location.href = config.LOCAL_AUTH_URL;
    return null;
  } else {
    return (
      <Route {...rest} render={props => <Component {...rest} {...props} />} />
    );
  }
};

export default PrivateRoute;
