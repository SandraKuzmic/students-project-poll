import React from "react";

import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import { COLORS } from "constants/colors";
import { assignUserToTopic, isStudentAssignedToTopic } from "services/topic";
import UnassignUserModalButton from "../components/Modals/UnassignUser/UnassignUser";
import ApproveTopicModalButton from "../components/Modals/ApproveTopic/ApproveTopic"
import DenyTopicModalButton from "../components/Modals/DenyTopic/DenyTopic";
import { dateFormatting} from "../services/helpers";

const useStyles = makeStyles({
  poll: {
    backgroundColor: COLORS.white,
    boxShadow: "0 3px 5px 2px rgba(0, 0, 0, .12)",
    borderRadius: 8,
    padding: 24,
    paddingBottom: 0,
    marginBottom: 32,
    cursor: "pointer"
  },
  disabledPoll: {
    backgroundColor: COLORS.gray_7,
    boxShadow: "0 3px 5px 2px rgba(0, 0, 0, .12)",
    borderRadius: 8,
    padding: 24,
    paddingBottom: 0,
    marginBottom: 32,
    cursor: "pointer"
  },
  button: {
    alignSelf: "flex-end",
    fontSize: 12
  },
  buttonContainer: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-end"
  },
  title: {
    fontWeight: 700,
    fontSize: 24
  },
  pollText: {
    fontSize: 14,
    marginBottom: 8,
    position: "relative",
    paddingLeft: 16,
    color: COLORS.gray_5
  },
  indented: {
    fontSize: 14,
    marginBottom: 8,
    position: "relative",
    paddingLeft: 32,
    color: COLORS.gray_5
  },
  topicBlock: {
    display: "flex",
    flexDirection: "row",
    paddingBottom: 24
  },
  topicBlockLeft: {
    flex: "1",
    borderRight: "1px solid #CCCCCC"
  },
  topicBlockRight: {
    flex: "1"
  },
  headerBlock: {
    display: "flex",
    flexDirection: "row",
    paddingBottom: 0
  },
  headerBlockRight: {
    flex: "1",
    textAlign: "right"
  },
  arrowWrapper: {
    textAlign: "center",
    paddingBottom: 8
  },
  arrowDown: {
    border: "solid",
    borderColor: COLORS.gray_6,
    borderWidth: "0 3px 3px 0",
    display: "inline-block",
    padding: 5,
    transform: "rotate(45deg)"
  },
  arrowUp: {
    border: "solid",
    borderColor: COLORS.gray_6,
    borderWidth: "0 3px 3px 0",
    display: "inline-block",
    padding: 5,
    transform: "rotate(-135deg)"
  },
  closed: {
    display: "none"
  },
  open: {
    display: "block"
  }
});

export default function Topic({
  topicId,
  userId,
  title,
  status,
  description,
  assignedUsers,
  maxStudentCount,
  topicOrganizationType,
  deadline,
  suggestedBy,
  startDay,
  user, userTopics
}) {
  const classes = useStyles(); 
  const [open, setOpen] = React.useState(false);

  let today = dateFormatting(new Date());

  const handleOpen = () => {
    setOpen(!open);
  };

  return (
    <Box
      className={status === "APPROVED" ? classes.poll : classes.disabledPoll}
    >
      <div className={classes.headerBlock}>
        <div className={classes.headerBlockLeft}>
          <Typography>
            Status:
            {status === "APPROVED" && " Odobreno"}
            {status === "WAITING_FOR_APPROVAL" && " Čeka odobrenje"}
            {status === "DENIED" && " Odbijeno"}
          </Typography>
          <Typography variant="h5" className={classes.title}>
            {title}
          </Typography>
        </div>
        <div className={classes.headerBlockRight}>
          <Typography className={classes.pollText}>
            Broj studenata: {assignedUsers.length}/{maxStudentCount}
          </Typography>
            {status === "APPROVED" && user.roles.indexOf("ROLE_ADMIN") === -1 && user.roles.indexOf("ROLE_MODERATOR") === -1
            && today >= dateFormatting(startDay) && today <= dateFormatting(deadline) && (
              <div className={classes.buttonContainer}>
                {!isStudentAssignedToTopic(assignedUsers, userId) && userTopics.length <= 0 && assignedUsers.length < maxStudentCount &&
                <Button
                    variant="outlined"
                    className={classes.button}
                    onClick={() => {
                      assignUserToTopic(userId, topicId);
                      window.location.reload();
                    }}
                >
                  Odaberi
                </Button>
                }
                {isStudentAssignedToTopic(assignedUsers, userId) &&
                  <>
                    <Button disabled variant={"outlined"}>
                      Prijavljen
                    </Button>
                    <UnassignUserModalButton topicId={topicId}  />
                  </>
                }
              </div>
            )}
          {status === "WAITING_FOR_APPROVAL" && user.roles.indexOf("ROLE_ADMIN") !== -1 && user.roles.indexOf("ROLE_MODERATOR") !== -1
          && today >= startDay && today <= deadline && (
            <div className={classes.buttonContainer}>
              <ApproveTopicModalButton topicId={topicId}/>
              <DenyTopicModalButton topicId={topicId} />
            </div>
          )}
        </div>
      </div>
      <div className={open ? classes.open : classes.closed}>
        <div className={classes.topicBlock}>
          <div className={classes.topicBlockLeft}>
            <Typography className={classes.pollText}>{topicOrganizationType}</Typography>
            <Typography className={classes.pollText}>{description}</Typography>
            <Typography className={classes.pollText}>
              Rok prijave: {dateFormatting(deadline)}
            </Typography>
          </div>
          <div className={classes.topicBlockRight}>
            <Typography className={classes.pollText}>
              Predložio: {suggestedBy}
            </Typography>
            <Typography className={classes.pollText}>Studenti:</Typography>

            {assignedUsers.map((student, index) => (
              <Typography className={classes.indented} key={index}>
                {student.name}
              </Typography>
            ))}
          </div>
        </div>
      </div>
      <div
        id="expand-arow"
        className={classes.arrowWrapper}
        onClick={handleOpen}
      >
        <span className={open ? classes.arrowUp : classes.arrowDown} />
      </div>
    </Box>
  );
}
