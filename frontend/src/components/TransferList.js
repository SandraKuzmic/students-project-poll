import React, { useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Checkbox from "@material-ui/core/Checkbox";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import { Typography } from "@material-ui/core";
import { assignUserToModerator, removeUserFromModerator } from "util/APIUtils";

const useStyles = makeStyles(theme => ({
  root: {
    margin: "auto"
  },
  paper: {
    width: 300,
    height: 400,
    overflow: "auto"
  },
  button: {
    margin: theme.spacing(0.5, 0)
  }
}));

function not(a, b) {
  return a.filter(value => b.indexOf(value) === -1);
}

function intersection(a, b) {
  return a.filter(value => b.indexOf(value) !== -1);
}

export default function TransferList({
  title1,
  title2,
  list1, // Assigned users
  list2, // Not assigned users
  activeModeratorId
}) {
  const classes = useStyles();
  const [checked, setChecked] = React.useState([]);
  const [left, setLeft] = React.useState(list1);
  const [right, setRight] = React.useState(list2);

  const leftChecked = intersection(checked, left);
  const rightChecked = intersection(checked, right);

  // DISCARD CHANGES ON NEW MODERATOR CLICKED
  useEffect(() => {
    setLeft(list1);
    setRight(list2);
  }, [activeModeratorId, list1, list2]);

  const handleToggle = value => () => {
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    setChecked(newChecked);
  };

  const handleCheckedLeft = () => {
    setLeft(left.concat(rightChecked));
    setRight(not(right, rightChecked));
    setChecked(not(checked, rightChecked));
  };

  const handleAllLeft = () => {
    setLeft(left.concat(right));
    setRight([]);
  };

  const handleAllRight = () => {
    setRight(right.concat(left));
    setLeft([]);
  };

  const handleSubmit = async () => {
    // 1. Remove users from poll
    const usersToRemove = list1.filter(user => !left.includes(user));
    for (let user of usersToRemove) {
      await removeUserFromModerator(user.id, activeModeratorId);
    }

    // 2. Assign users to poll
    const usersToAssign = left.filter(user => !list1.includes(user));
    for (let user of usersToAssign) {
      await assignUserToModerator(user.id, activeModeratorId);
    }
  };

  const handleCheckedRight = () => {
    setRight(right.concat(leftChecked));
    setLeft(not(left, leftChecked));
    setChecked(not(checked, leftChecked));
  };

  const customList = users => (
    <Paper className={classes.paper}>
      <List dense component="div" role="list">
        {users.map((user, index) => {
          const labelId = `transfer-list-item-${index}-label`;

          return (
            <ListItem
              key={index}
              role="listitem"
              button
              onClick={handleToggle(user)}
            >
              <ListItemIcon>
                <Checkbox
                  checked={checked.indexOf(user) !== -1}
                  tabIndex={-1}
                  disableRipple
                  inputProps={{ "aria-labelledby": labelId }}
                />
              </ListItemIcon>
              <ListItemText id={labelId} primary={user.name} />
            </ListItem>
          );
        })}
        <ListItem />
      </List>
    </Paper>
  );

  return (
    <Grid
      container
      spacing={2}
      justify="center"
      alignItems="center"
      className={classes.root}
    >
      <Grid item>
        <Paper elevation={2} style={{ backgroundColor: "lightgray" }}>
          <Typography variant="h6" style={{ textAlign: "center" }}>
            {title1}
          </Typography>
          {customList(left)}
        </Paper>
      </Grid>
      <Grid item>
        <Grid container direction="column" alignItems="center">
          <Button
            variant="outlined"
            size="small"
            className={classes.button}
            onClick={handleAllRight}
            disabled={left.length === 0}
            aria-label="move all right"
          >
            ≫
          </Button>
          <Button
            variant="outlined"
            size="small"
            className={classes.button}
            onClick={handleCheckedRight}
            disabled={leftChecked.length === 0}
            aria-label="move selected right"
          >
            &gt;
          </Button>

          <Button
            variant="outlined"
            size="small"
            className={classes.button}
            onClick={handleCheckedLeft}
            disabled={rightChecked.length === 0}
            aria-label="move selected left"
          >
            &lt;
          </Button>
          <Button
            variant="outlined"
            size="small"
            className={classes.button}
            onClick={handleAllLeft}
            disabled={right.length === 0}
            aria-label="move all left"
          >
            ≪
          </Button>
        </Grid>
      </Grid>
      <Grid item>
        <Paper elevation={2} style={{ backgroundColor: "lightgray" }}>
          <Typography variant="h6" style={{ textAlign: "center" }}>
            {title2}
          </Typography>
          {customList(right)}
        </Paper>
      </Grid>

      <button
        type={"button"}
        className="btn btn-primary btn-lg btn-block"
        onClick={handleSubmit}
      >
        Pohrani promjene
      </button>
    </Grid>
  );
}
