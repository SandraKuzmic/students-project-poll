import { TOPICS } from "mock_data/topics";

export const POLLS = [
  {
    id: 0,
    title: "Diplomski seminar",
    filledTopicCount: 5,
    topicCount: 25,
    studentCount: 12,
    maxStudentCount: 12,
    deadline: new Date("08/11/2019"),
    topics: TOPICS
  },
  {
    id: 1,
    title: "Projekt iz programske potpore",
    filledTopicCount: 10,
    topicCount: 30,
    studentCount: 23,
    maxStudentCount: 150,
    deadline: new Date("07/10/2019"),
    topics: TOPICS
  },
  {
    id: 2,
    title: "Diplomski rad",
    filledTopicCount: 10,
    topicCount: 10,
    studentCount: 25,
    maxStudentCount: 25,
    deadline: new Date(),
    topics: TOPICS
  }
];
