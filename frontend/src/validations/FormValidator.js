class FormValidator {
  constructor() {
    this.title = { valid: false, message: "Upišite naslov" };
    this.minNumber = {
      valid: false,
      message: "Upišite minimalni broj studenata"
    };
    this.maxNumber = {
      valid: false,
      message: "Upišite maksimalni broj studenata"
    };
    this.number = {
      valid: false,
      message: "Maksimalni broj studenata mora biti veći od minimalnog"
    };
    this.date = {
      valid: false,
      message: "Datum završetka prijava mora biti poslije datuma početka"
    };
  }

  validatePoll(title, minNumber, maxNumber, fromDate, toDate) {
    this.validateTitle(title);
    this.validateMinNumber(minNumber);
    this.validateMaxNumber(maxNumber);
    this.validateNumber(minNumber, maxNumber);
    this.validateDate(fromDate, toDate);

    if (
      this.title.valid &&
      this.minNumber.valid &&
      this.number.valid &&
      this.date.valid
    ) {
      return true;
    }
    return false;
  }

  validateTitle(title) {
    if (title) {
      this.title.value = title;
      this.title.valid = true;
      return true;
    }
    return false;
  }

  validateMinNumber(minNumber) {
    if (minNumber > 0) {
      this.minNumber.value = minNumber;
      this.minNumber.valid = true;
      return true;
    }
    return false;
  }

  validateMaxNumber(maxNumber) {
    if (maxNumber > 0) {
      this.maxNumber.value = maxNumber;
      this.maxNumber.valid = true;
      return true;
    }
    return false;
  }

  validateNumber(minNumber, maxNumber) {
    if (maxNumber === 0 || maxNumber >= minNumber) {
      this.number.valid = true;
      return true;
    }
    return false;
  }

  validateDate(fromDate, toDate) {
    if (fromDate.getTime() <= toDate.getTime()) {
      this.date.valid = true;
      return true;
    }
    return false;
  }
}

export default FormValidator;
