import React, {useEffect, useState} from "react";

import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import {makeStyles} from "@material-ui/core/styles";
import Topic from "components/Topic";
import {COLORS} from "constants/colors";
import {getTopicsAssignedToUser} from "services/topic";
import {getCurrentUser} from "../util/APIUtils";

const useStyles = makeStyles({
  root: {},
  title: {
    fontSize: 40,
    padding: 30,
    fontWeight: 400,
    paddingLeft: 0
  },
  row: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    paddingTop: 32,
    paddingBottom: 32
  },
  buttons: {
    display: "flex"
  },
  pollText: {
    fontSize: 14,
    marginBottom: 8,
    position: "relative",
    color: COLORS.gray_5
  },
  button: {
    height: 40
  }
});

export default function MyTopicScreen({ user }) {
  const classes = useStyles();
  const [topics, setTopics] = useState([]);

  useEffect(() => {
    getCurrentUser().then(user => {
      getTopicsAssignedToUser(user.id).then(topics => setTopics(topics));
    });
  }, []);

  const renderTopics = () =>
    topics.map(topic => (
      <Topic
        key={topic.id}
        userId={user.id}
        title={topic.title}
        description={topic.description}
        status={topic.status}
        type={topic.type}
        assignedUsers={topic.assignedUsers}
        maxStudentCount={topic.maxNumberOfStudents}
        deadline={topic.applicationEnd}
        suggestedBy={topic.createdBy.name}
        startDay={topic.applicationStart}
        user={user}
      />
    ));

  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="md" className={classes.root}>
        <div className={classes.row}>
          <Typography variant="h5">Teme:</Typography>
        </div>
        {renderTopics()}
      </Container>
    </React.Fragment>
  );
}
