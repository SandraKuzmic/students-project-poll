import React, { useEffect, useState } from "react";
import UserList from "components/UserList";
import {
  fetchAllUsers,
  fetchAssignedUsers
  // fetchUsersByRole
} from "util/APIUtils";
import TransferList from "components/TransferList";
import Paper from "@material-ui/core/Paper";
import { Typography } from "@material-ui/core";

export default function AdminAssign({ currentUser }) {
  const [moderators, setModerators] = useState([]);
  const [students, setStudents] = useState([]);
  const [assignedStudents, setAssignedStudents] = useState([]);
  const [otherStudents, setOtherStudents] = useState([]);
  const [activeModeratorId, setActiveModeratorId] = useState();

  useEffect(() => {
    fetchAllUsers().then(users => {
      setModerators(users.filter(user => hasRole(user, "ROLE_MODERATOR")));
      setStudents(users.filter(user => hasRole(user, "ROLE_USER")));
    });
    // fetchUsersByRole("ROLE_MODERATOR").then(setModerators); //NOT WORKING
    // fetchUsersByRole("ROLE_USER").then(setStudents); //NOT WORKING
  }, []);

  useEffect(() => {
    if (activeModeratorId) {
      fetchAssignedUsers(activeModeratorId).then(assignedUsers => {
        setAssignedStudents(assignedUsers);

        let otherStudents = students.filter(st => {
          for (let student of assignedUsers) {
            if (st.id === student.id) return false;
          }

          return true;
        });
        setOtherStudents(otherStudents);
      });
    }
  }, [activeModeratorId, students]);

  function hasRole(user, targetRole) {
    for (let roleObj of user.roles) {
      if (roleObj["role"] === targetRole) return true;
    }
    return false;
  }

  function isAdmin() {
    if (typeof currentUser.roles !== "undefined") {
      return currentUser.roles.indexOf("ROLE_ADMIN") !== -1;
    }
    return false;
  }

  const activeModerator = moderators.filter(
    m => m.id === parseInt(activeModeratorId)
  )[0];

  return isAdmin() ? (
    <div className="container" style={{ marginTop: 200 }}>
      <div className="row">
        <div className="col-4">
          <UserList
            title="Moderatori"
            users={moderators}
            activeUser={activeModeratorId}
            setActiveUser={setActiveModeratorId}
          />
        </div>

        {activeModeratorId && (
          <div className="col-8">
            <Paper variant="outlined">
              <Typography variant="h5" style={{ textAlign: "center" }}>
                Moderator: {activeModerator.name}
              </Typography>

              <TransferList
                title1={"Pridruženi studenti"}
                title2={"Ostali studenti"}
                list1={assignedStudents}
                list2={otherStudents}
                activeModeratorId={activeModeratorId}
              />
            </Paper>
          </div>
        )}
      </div>
    </div>
  ) : (
    <div
      className="alert alert-danger"
      role="alert"
      style={{ marginTop: 100, textAlign: "center" }}
    >
      You need to be logged in as an admin to access this resource!
    </div>
  );
}
