import React, { useEffect, useState } from "react";

import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import { makeStyles } from "@material-ui/core/styles";
import Poll from "components/Poll";
import { fetchPollsForUser } from "services/poll";
import AddPollModalButton from "components/Modals/AddPoll/AddPoll";
import { getCurrentUser } from "util/APIUtils";
import { ACCESS_TOKEN, config } from "../constants";

const useStyles = makeStyles({
  root: {},
  title: {
    fontSize: 40,
    fontWeight: 400,
    padding: 30,
    paddingLeft: 0
  },
  row: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center"
  },
  button: {
    height: 40
  }
});

export default function HomeScreen() {
  const classes = useStyles();
  const [polls, setPolls] = useState([]);
  const [user, setUser] = useState({});

  useEffect(() => {
    getCurrentUser().then(user => {
      setUser(user);
      fetchPollsForUser(user.id, "hr")
        .then(polls => setPolls(polls))
        .catch(console.error);
    });
  }, []);

  function isAdmin() {
    if (typeof user.roles !== "undefined") {
      return user.roles.indexOf("ROLE_ADMIN") !== -1;
    }
    return false;
  }

  function isModerator() {
    if (typeof user.roles !== "undefined") {
      return user.roles.indexOf("ROLE_MODERATOR") !== -1;
    }
    return false;
  }

  const renderPolls = () => {
    return polls.map(poll => {
      let numberOfAssigned = poll.topics.reduce(
        (agg, cur) => agg + cur.assignedUsers.length,
        0
      );
      let maxStudents = poll.topics.reduce(
        (agg, cur) => agg + cur.maxNumberOfStudents,
        0
      );
      let filledTopics = poll.topics.filter(
        topic => topic.assignedUsers.length >= topic.maxNumberOfStudents
      );

      return (
        <Poll
          key={poll.id}
          id={poll.id}
          title={poll.title}
          description={poll.description}
          filledTopicCount={filledTopics.length}
          topicCount={poll.topics.length}
          studentCount={numberOfAssigned}
          maxStudentCount={maxStudents}
          deadline={poll["applicationEnd"]}
        />
      );
    });
  };

  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="sm" className={classes.root}>
        <div className={classes.row}>
          <Typography variant="h2" className={classes.title}>
            Moji projekti
          </Typography>

          {(isAdmin() || isModerator()) ? <AddPollModalButton /> : null}
        </div>
        {renderPolls()}
      </Container>
    </React.Fragment>
  );
}
