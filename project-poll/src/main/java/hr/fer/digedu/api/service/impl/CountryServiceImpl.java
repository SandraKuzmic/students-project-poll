package hr.fer.digedu.api.service.impl;

import hr.fer.digedu.api.model.Country;
import hr.fer.digedu.api.repository.CountryRepository;
import hr.fer.digedu.api.service.CountryService;
import java.util.List;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Service;

@Service
public class CountryServiceImpl implements CountryService {

    private final CountryRepository countryRepository;

    public CountryServiceImpl(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    @Override
    public List<Country> getAll() {
        return countryRepository.findAll();
    }

    @Override
    public Country getByCode(String code) {
        return (Country) Hibernate.unproxy(countryRepository.getOne(code.toUpperCase()));
    }
}
