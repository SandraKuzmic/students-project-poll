package hr.fer.digedu.api.service.impl;

import hr.fer.digedu.api.configuration.security.model.UserPrincipal;
import hr.fer.digedu.api.exception.ResourceNotFoundException;
import hr.fer.digedu.api.model.User;
import hr.fer.digedu.api.repository.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    public CustomUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String email)
            throws UsernameNotFoundException {
        User user = userRepository.findByEmail(email)
                                             .orElseThrow(() ->
                        new UsernameNotFoundException("User not found with email : " + email)
        );

        return UserPrincipal.create(user);
    }

    @Transactional
    public UserDetails loadUserById(Long id) throws ResourceNotFoundException {
        User user = userRepository.getOne(id);
        return UserPrincipal.create(user);
    }
}
