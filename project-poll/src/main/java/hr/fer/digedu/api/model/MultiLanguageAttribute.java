package hr.fer.digedu.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@SequenceGenerator(sequenceName = "multi_language_id_seq", name= "multilanguageSeq", allocationSize = 1)
public abstract class MultiLanguageAttribute {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "multilanguageSeq")
    private Long id;

    @ManyToOne(targetEntity = Country.class)
    private Country language;

    @Column(nullable = false)
    private String value;

    public Long getId() {
        return id;
    }

    public MultiLanguageAttribute setId(Long id) {
        this.id = id;
        return this;
    }

    public Country getLanguage() {
        return language;
    }

    public MultiLanguageAttribute setLanguage(Country language) {
        this.language = language;
        return this;
    }

    public String getValue() {
        return value;
    }

    public MultiLanguageAttribute setValue(String value) {
        this.value = value;
        return this;
    }

}
