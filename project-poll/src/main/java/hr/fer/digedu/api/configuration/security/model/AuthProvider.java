package hr.fer.digedu.api.configuration.security.model;

public enum AuthProvider {
    local, facebook
}
