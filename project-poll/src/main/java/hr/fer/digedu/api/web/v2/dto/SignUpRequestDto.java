package hr.fer.digedu.api.web.v2.dto;

import javax.validation.constraints.NotBlank;

public class SignUpRequestDto {

    @NotBlank
    private String name;

    @NotBlank
//    @Email
    private String username;

    @NotBlank
    private String password;

    public SignUpRequestDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
