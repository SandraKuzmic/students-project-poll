package hr.fer.digedu.api.web.v1;

import hr.fer.digedu.api.model.User;
import hr.fer.digedu.api.service.UserService;
import hr.fer.digedu.api.web.v2.dto.UserDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import java.util.Set;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import org.springframework.http.ResponseEntity;
import static org.springframework.http.ResponseEntity.ok;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/api/v1/user")
@RestController
@Api(value = "/api/v1/user", tags = { "User" }, produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
public class UserControllerV1 {

    private UserService    userService;

    public UserControllerV1(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    @ApiOperation(value = "Gets all users", response = User.class, responseContainer = "List")
    public ResponseEntity<List<User>> getAllUsers() {
        return ok(userService.getAll());
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Gets single user", response = User.class)
    public ResponseEntity<User> getUserById(@PathVariable("id") Long id) {
        return ok(userService.getById(id));
    }

    @GetMapping("/{id}/assigned")
    @ApiOperation(value = "Fetches all users assigned to user with id", response = UserDto.class,
            responseContainer = "List")
    public ResponseEntity<Set<User>> getAssignedUsersForUser(@PathVariable("id") Long userId) {
        return ok(userService.getAssignedUsersForModerator(userId));
    }

    @GetMapping("/filter/role/{role_name}")
    public ResponseEntity<List<User>> getUsersByRole(@PathVariable("role_name") String roleName) {
        return ResponseEntity.ok(userService.getUsersByRole(roleName));
    }

    @PostMapping("/{assignerId}/assign/{assigneeId}")
    @ApiOperation(value = "Assign user to moderator", response = UserDto.class)
    public ResponseEntity<User> assignUserToModerator(@PathVariable("assignerId") Long assignerId,
            @PathVariable("assigneeId") Long assigneeId) {
        User assigner = userService.getById(assignerId);
        User assignee = userService.getById(assigneeId);

        assigner = userService.assignUserToModerator(assignee, assigner);

        return assigner == null ? ResponseEntity.badRequest().build() : ResponseEntity.ok(assigner);
    }

    @PostMapping("/{assignerId}/remove/{assigneeId}")
    @ApiOperation(value = "Assign user to moderator", response = UserDto.class)
    public ResponseEntity<User> unassignUserFromModerator(@PathVariable("assignerId") Long assignerId,
            @PathVariable("assigneeId") Long assigneeId) {
        User assigner = userService.getById(assignerId);
        User assignee = userService.getById(assigneeId);

        assigner = userService.unassignUserFromModerator(assignee, assigner);

        return ResponseEntity.ok(assigner);
    }

}
