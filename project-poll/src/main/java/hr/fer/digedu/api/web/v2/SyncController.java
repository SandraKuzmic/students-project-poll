package hr.fer.digedu.api.web.v2;

import hr.fer.digedu.api.service.SyncService;
import io.swagger.annotations.Api;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v2/sync")
@Api(value = "/api/v2/sync", tags = { "Sync" }, produces = APPLICATION_JSON_VALUE,
        consumes = APPLICATION_JSON_VALUE)
public class SyncController {

    private final SyncService syncService;

    public SyncController(SyncService syncService) {
        this.syncService = syncService;
    }

    @GetMapping("/user")
    private ResponseEntity<?> startUserSync() {
        syncService.syncUsers();
        return ResponseEntity.accepted().build();
    }

}
