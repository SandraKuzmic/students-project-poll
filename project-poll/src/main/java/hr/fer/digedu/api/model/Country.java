package hr.fer.digedu.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Country {

    public static final String DEFAULT_LANGUAGE = "HR";

    @Id
    @Column(length = 3)
    private String code;

    @Column(nullable = false, updatable = false, length = 80)
    private String name;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
