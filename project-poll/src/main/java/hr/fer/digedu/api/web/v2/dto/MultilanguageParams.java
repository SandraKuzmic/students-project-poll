package hr.fer.digedu.api.web.v2.dto;

import hr.fer.digedu.api.model.Country;
import hr.fer.digedu.api.model.MultiLanguageAttribute;
import hr.fer.digedu.api.service.CountryService;

public interface MultilanguageParams {

    MultilanguageParamDto getName();

    void setName(MultilanguageParamDto name);

    MultilanguageParamDto getDescription();

    void setDescription(MultilanguageParamDto description);

    static void setLanguageForAttributes(MultilanguageParams attribute,
            MultiLanguageAttribute name, MultiLanguageAttribute description, CountryService countryService) {
        String nameLang = attribute.getName().getCode();
        String descriptionLang = attribute.getDescription().getCode();

        if (!nameLang.equalsIgnoreCase(descriptionLang)) {
            throw new IllegalArgumentException("Title language code and description language code doesn't match");
        }

        Country language = countryService.getByCode(attribute.getName().getCode());
        name.setLanguage(language);
        description.setLanguage(language);
    }

}
