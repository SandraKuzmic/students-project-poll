package hr.fer.digedu.api.model.poll;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import hr.fer.digedu.api.model.TopicOrganizationType;
import static hr.fer.digedu.api.model.TopicOrganizationType.PARALLEL;
import hr.fer.digedu.api.model.User;
import hr.fer.digedu.api.model.topic.Topic;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Set;
import javax.persistence.*;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
@SequenceGenerator(name = "pollSeq", sequenceName = "poll_id_seq", allocationSize = 1)
public class Poll {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pollSeq")
    private Long id;

    @Column(name = "titles", unique = true)
    @OneToMany(targetEntity = MultiLanguagePollTitle.class, cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    private Set<MultiLanguagePollTitle> titles;

    @Column(name = "descriptions", unique = true)
    @OneToMany(targetEntity = MultiLanguagePollDescription.class, cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    private Set<MultiLanguagePollDescription> descriptions;

    @OneToMany(mappedBy = "poll", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    @JsonIgnore
    private Set<Topic> topics;

    @ManyToOne
    @JoinColumn(name = "created_by", nullable = false)
    private User createdBy;

    @Column(name = "allow_unregister")
    private Boolean allowUnregister = false;

    @Column(name = "allow_students_add_topic", nullable = false)
    private Boolean allowStudentsAddTopic = false;

    @Column(name = "topic_organization_type", nullable = false)
    private TopicOrganizationType topicOrganizationType = PARALLEL;

    @Column(name = "min_num_of_students")
    private Integer minNumberOfStudents;

    @Column(name = "max_num_of_students", nullable = false)
    private Integer maxNumberOfStudents;

    @Column(name = "application_start")
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate applicationStart;

    @Column(nullable = false)
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate applicationEnd;

    @CreationTimestamp
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createdAt;

    @UpdateTimestamp
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updatedAt;

    @ManyToMany(targetEntity = User.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<User> accessPollUsers;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<MultiLanguagePollTitle> getTitles() {
        return titles;
    }

    public void setTitles(Set<MultiLanguagePollTitle> title) {
        this.titles = title;
    }

    public Set<MultiLanguagePollDescription> getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(Set<MultiLanguagePollDescription> description) {
        this.descriptions = description;
    }

    public Set<Topic> getTopics() {
        return topics;
    }

    public void setTopics(Set<Topic> topics) {
        this.topics = topics;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean getAllowStudentsAddTopic() {
        return allowStudentsAddTopic;
    }

    public void setAllowStudentsAddTopic(Boolean allowStudentsAddTopic) {
        this.allowStudentsAddTopic = allowStudentsAddTopic;
    }

    public TopicOrganizationType getTopicOrganizationType() {
        return topicOrganizationType;
    }

    public void setTopicOrganizationType(TopicOrganizationType topicOrganizationType) {
        this.topicOrganizationType = topicOrganizationType;
    }

    public LocalDate getApplicationStart() {
        return applicationStart;
    }

    public void setApplicationStart(LocalDate applicationStart) {
        this.applicationStart = applicationStart;
    }

    public LocalDate getApplicationEnd() {
        return applicationEnd;
    }

    public void setApplicationEnd(LocalDate applicationEnd) {
        this.applicationEnd = applicationEnd;
    }

    public Boolean getAllowUnregister() {
        return allowUnregister;
    }

    public void setAllowUnregister(Boolean allowUnregister) {
        this.allowUnregister = allowUnregister;
    }

    public Integer getMinNumberOfStudents() {
        return minNumberOfStudents;
    }

    public void setMinNumberOfStudents(Integer minNumberOfStudents) {
        this.minNumberOfStudents = minNumberOfStudents;
    }

    public Integer getMaxNumberOfStudents() {
        return maxNumberOfStudents;
    }

    public void setMaxNumberOfStudents(Integer maxNumberOfStudents) {
        this.maxNumberOfStudents = maxNumberOfStudents;
    }

    public Set<User> getAccessPollUsers() {
        return accessPollUsers;
    }

    public void setAccessPollUsers(Set<User> accessPollUsers) {
        this.accessPollUsers = accessPollUsers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Poll poll = (Poll) o;
        return Objects.equals(id, poll.id) && Objects.equals(titles, poll.titles) &&
                Objects.equals(descriptions, poll.descriptions) && Objects.equals(topics, poll.topics);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, titles, descriptions, topics);
    }

}
