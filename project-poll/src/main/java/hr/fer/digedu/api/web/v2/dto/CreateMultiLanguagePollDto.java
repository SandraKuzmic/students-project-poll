package hr.fer.digedu.api.web.v2.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import hr.fer.digedu.api.model.TopicOrganizationType;
import java.time.LocalDate;
import java.util.List;
import javax.validation.constraints.Future;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class CreateMultiLanguagePollDto {

    @NotNull
    @NotEmpty
    private List<PollMultilanguageParams> multilanguageAttributes;

    @NotNull
    private Long userId;

    @NotNull
    private Boolean allowUnregister = false;

    @NotNull
    private Boolean allowStudentsAddTopic;

    @Positive
    private Integer minNumberOfStudents;

    @Positive
    private Integer maxNumberOfStudents;

    @FutureOrPresent
    @NotNull
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate applicationStart;

    @Future
    @NotNull
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate applicationEnd;

    @NotNull
    private TopicOrganizationType topicOrganizationType;

    public List<PollMultilanguageParams> getMultilanguageAttributes() {
        return multilanguageAttributes;
    }

    public void setMultilanguageAttributes(List<PollMultilanguageParams> multilanguageAttributes) {
        this.multilanguageAttributes = multilanguageAttributes;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Boolean getAllowUnregister() {
        return allowUnregister;
    }

    public void setAllowUnregister(Boolean allowUnregister) {
        this.allowUnregister = allowUnregister;
    }

    public Boolean getAllowStudentsAddTopic() {
        return allowStudentsAddTopic;
    }

    public void setAllowStudentsAddTopic(Boolean allowStudentsAddTopic) {
        this.allowStudentsAddTopic = allowStudentsAddTopic;
    }

    public Integer getMinNumberOfStudents() {
        return minNumberOfStudents;
    }

    public void setMinNumberOfStudents(Integer minNumberOfStudents) {
        this.minNumberOfStudents = minNumberOfStudents;
    }

    public Integer getMaxNumberOfStudents() {
        return maxNumberOfStudents;
    }

    public void setMaxNumberOfStudents(Integer maxNumberOfStudents) {
        this.maxNumberOfStudents = maxNumberOfStudents;
    }

    public LocalDate getApplicationStart() {
        return applicationStart;
    }

    public void setApplicationStart(LocalDate applicationStart) {
        this.applicationStart = applicationStart;
    }

    public LocalDate getApplicationEnd() {
        return applicationEnd;
    }

    public void setApplicationEnd(LocalDate applicationEnd) {
        this.applicationEnd = applicationEnd;
    }

    public TopicOrganizationType getTopicOrganizationType() {
        return topicOrganizationType;
    }

    public void setTopicOrganizationType(TopicOrganizationType topicOrganizationType) {
        this.topicOrganizationType = topicOrganizationType;
    }

    public static class PollMultilanguageParams implements MultilanguageParams {

        @NotNull
        private MultilanguageParamDto name;
        @NotNull
        private MultilanguageParamDto description;

        public PollMultilanguageParams() {
        }

        public PollMultilanguageParams(MultilanguageParamDto title, MultilanguageParamDto description) {
            this.name = title;
            this.description = description;
        }

        public MultilanguageParamDto getName() {
            return name;
        }

        public void setName(MultilanguageParamDto name) {
            this.name = name;
        }

        public MultilanguageParamDto getDescription() {
            return description;
        }

        public void setDescription(MultilanguageParamDto description) {
            this.description = description;
        }

    }

}
