package hr.fer.digedu.api.service;

import hr.fer.digedu.api.model.Country;
import java.util.List;

/**
 * Country service implements methods for fetching countries data. It doesn't allow users to insert new countries
 * because its expected that country list is preloaded in database.
 */
public interface CountryService {

    /**
     * Gets all countries
     *
     * @return List of {@link Country} objects
     */
    List<Country> getAll();

    /**
     * Get single country by code
     *
     * @param code of the country
     * @return {@link Country} if found
     * @throws javax.persistence.EntityNotFoundException if there is no country with given code
     */
    Country getByCode(String code);
}
