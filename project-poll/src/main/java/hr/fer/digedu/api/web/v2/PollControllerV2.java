package hr.fer.digedu.api.web.v2;

import hr.fer.digedu.api.model.poll.Poll;
import hr.fer.digedu.api.repository.dbo.PollDBO;
import hr.fer.digedu.api.repository.dbo.TopicDBO;
import hr.fer.digedu.api.service.PollService;
import hr.fer.digedu.api.service.TopicService;
import hr.fer.digedu.api.web.v2.dto.CreateMultiLanguagePollDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.validation.Valid;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RequestMapping("/api/v2/poll")
@RestController
@Api(value = "/api/v2/poll", tags = { "Poll" }, produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
public class PollControllerV2 {

    private PollService  pollService;
    private TopicService topicService;

    public PollControllerV2(PollService pollService, TopicService topicService) {
        this.pollService = pollService;
        this.topicService = topicService;
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Finds all polls", response = Poll.class, responseContainer = "List")
    public ResponseEntity<List<PollDBO>> getAllPolls(@RequestParam(required = false, value = "lang") String language) {
        return ResponseEntity.ok(pollService.getAllByLanguage(language));
    }

    @GetMapping(value = "/{id}", produces = APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Find poll with given id", response = Poll.class)
    public ResponseEntity<PollDBO> getOne(@PathVariable("id") Long id,
            @RequestParam(required = false, value = "lang") String language) {
        return ResponseEntity.ok(pollService.getByIdAndLanguage(id, language));
    }

    @PostMapping(produces = APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Creates new poll", response = Poll.class)
    public ResponseEntity<Poll> createPoll(@Valid @RequestBody CreateMultiLanguagePollDto createMultiLanguagePollDto) {
        return ResponseEntity.ok(pollService.create(createMultiLanguagePollDto));
    }

    @GetMapping("/user/{userId}")
    @ApiOperation(value = "Finds all polls that are created by user", response = PollDBO.class,
            responseContainer = "List")
    public ResponseEntity<List<PollDBO>> getPollsForUser(@PathVariable("userId") Long userId,
            @RequestParam(required = false, value = "lang") String language) {
        return ResponseEntity.ok(pollService.getPollsCreatedByUserByLanguage(userId, language));
    }

    @PutMapping("/{pollId}/assign/{userId}")
    @ApiOperation(value = "Grant poll access to single user", response = Poll.class)
    public ResponseEntity<Poll> grantPollAccess(@PathVariable("pollId") Long pollId,
            @PathVariable("userId") Long userId) {
        return ResponseEntity.ok(pollService.grantAccessToUser(pollId, userId));
    }

    @PutMapping("/{pollId}/unassign/{userId}")
    @ApiOperation(value = "Remove poll access from single user", response = PollDBO.class)
    public ResponseEntity<PollDBO> revokePollAccess(@PathVariable("pollId") Long pollId,
            @PathVariable("userId") Long userId) {
        return ResponseEntity.ok(pollService.revokeAccessFromUser(pollId, userId));
    }

    @PutMapping("/{pollId}/assign")
    @ApiOperation(value = "Grant poll access to users", response = Poll.class)
    public ResponseEntity<Poll> grantPollAccess(@PathVariable("pollId") Long pollId,
            @RequestParam("users") MultipartFile usersFile) {
        return ResponseEntity.ok(pollService.grantAccessToUsers(pollId, usersFile));
    }

    @GetMapping("/assigned/{userId}")
    @ApiOperation(value = "Get all polls that user has access to", response = PollDBO.class)
    public ResponseEntity<List<PollDBO>> getPollAssignedToUser(@PathVariable("userId") Long userId,
            @RequestParam(required = false, value = "lang") String language) {
        return ResponseEntity.ok(pollService.getAccessiblePollsByLanguage(userId, language));
    }

    @GetMapping("/{id}/topics")
    @ApiOperation(value = "Get all topics for requested poll", response = PollDBO.class)
    public ResponseEntity<List<TopicDBO>> getTopicsForPoll(@PathVariable("id") Long pollId,
            @RequestParam(required = false, value = "lang") String language) {
        return ResponseEntity.ok(topicService.getTopicsForPollByLanguage(pollId, language));
    }

}
