package hr.fer.digedu.api.model.poll;

import hr.fer.digedu.api.model.MultiLanguageAttribute;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class MultiLanguagePollDescription extends MultiLanguageAttribute {
}
