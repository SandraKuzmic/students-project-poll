package hr.fer.digedu.api.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;
import static hr.fer.digedu.api.model.Country.DEFAULT_LANGUAGE;
import hr.fer.digedu.api.model.MultiLanguageAttribute;
import hr.fer.digedu.api.model.TopicOrganizationType;
import hr.fer.digedu.api.model.User;
import hr.fer.digedu.api.model.poll.MultiLanguagePollDescription;
import hr.fer.digedu.api.model.poll.MultiLanguagePollTitle;
import hr.fer.digedu.api.model.poll.Poll;
import hr.fer.digedu.api.repository.PollRepository;
import hr.fer.digedu.api.repository.dbo.PollDBO;
import static hr.fer.digedu.api.repository.dbo.PollDBO.mapPollToDBO;
import hr.fer.digedu.api.service.CountryService;
import hr.fer.digedu.api.service.PollService;
import hr.fer.digedu.api.service.UserService;
import hr.fer.digedu.api.web.v1.dto.CreatePollDto;
import hr.fer.digedu.api.web.v2.dto.CreateMultiLanguagePollDto;
import static hr.fer.digedu.api.web.v2.dto.CreateMultiLanguagePollDto.PollMultilanguageParams;
import hr.fer.digedu.api.web.v2.dto.MultilanguageParamDto;
import static hr.fer.digedu.api.web.v2.dto.MultilanguageParams.setLanguageForAttributes;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.hibernate.Hibernate;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class PollServiceImpl implements PollService {

    private final PollRepository pollRepository;

    private final UserService        userService;
    private final CountryService     countryService;
    private final ObjectMapper       objectMapper;
    private final ApplicationContext applicationContext;

    public PollServiceImpl(PollRepository pollRepository, UserService userService, CountryService countryService,
            ObjectMapper objectMapper, ApplicationContext applicationContext) {
        this.pollRepository = pollRepository;
        this.userService = userService;
        this.countryService = countryService;
        this.objectMapper = objectMapper;
        this.applicationContext = applicationContext;
    }

    @Override
    public List<PollDBO> getAll() {
        return applicationContext.getBean(PollService.class).getAllByLanguage(DEFAULT_LANGUAGE);
    }

    @Override
    public List<PollDBO> getAllByLanguage(String language) {
        final String lang = language == null ? DEFAULT_LANGUAGE : language;
        List<Poll> polls = pollRepository.findAll();
        return polls.stream().map(poll -> mapPollToDBO(lang, poll, objectMapper)).filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    @Override
    public PollDBO getById(Long id) {
        return applicationContext.getBean(PollService.class).getByIdAndLanguage(id, null);
    }

    @Override
    public PollDBO getByIdAndLanguage(Long id, String language) {
        final String lang = language == null ? DEFAULT_LANGUAGE : language;
        Poll poll = pollRepository.getOne(id);

        poll = (Poll) Hibernate.unproxy(poll);
        return mapPollToDBO(lang, poll, objectMapper);
    }

    @Override
    public Poll create(CreatePollDto createPollDto) {
        CreateMultiLanguagePollDto pollDto = new CreateMultiLanguagePollDto();

        pollDto.setAllowUnregister(createPollDto.getAllowUnregister());
        pollDto.setAllowStudentsAddTopic(createPollDto.getAllowStudentsAddTopic());
        pollDto.setApplicationStart(createPollDto.getApplicationStart());
        pollDto.setApplicationEnd(createPollDto.getApplicationEnd());
        pollDto.setMaxNumberOfStudents(createPollDto.getMaxNumberOfStudents());
        pollDto.setMinNumberOfStudents(createPollDto.getMinNumberOfStudents());
        pollDto.setTopicOrganizationType(TopicOrganizationType.PARALLEL);
        pollDto.setUserId(createPollDto.getUserId());

        MultilanguageParamDto title = new MultilanguageParamDto();
        MultilanguageParamDto description = new MultilanguageParamDto();

        title.setCode(DEFAULT_LANGUAGE);
        description.setCode(DEFAULT_LANGUAGE);

        title.setValue(createPollDto.getTitle());
        description.setValue(createPollDto.getDescription());

        pollDto.setMultilanguageAttributes(ImmutableList.of(new PollMultilanguageParams(title, description)));

        return applicationContext.getBean(PollService.class).create(pollDto);
    }

    @Override
    public Poll create(CreateMultiLanguagePollDto createMultiLanguagePollDto) {
        User user = userService
                .getById(createMultiLanguagePollDto.getUserId()); // TODO: fetch current user from security context
        Poll poll = objectMapper.convertValue(createMultiLanguagePollDto, Poll.class);

        HashSet<MultiLanguagePollTitle> titleList = new HashSet<>();
        HashSet<MultiLanguagePollDescription> descriptionList = new HashSet<>();
        for (PollMultilanguageParams attribute : createMultiLanguagePollDto
                .getMultilanguageAttributes()) {
            MultiLanguageAttribute name = new MultiLanguagePollTitle().setValue(attribute.getName().getValue());
            MultiLanguageAttribute description =
                    new MultiLanguagePollDescription().setValue(attribute.getDescription().getValue());

            setLanguageForAttributes(attribute, name, description, countryService);
            titleList.add((MultiLanguagePollTitle) name);
            descriptionList.add((MultiLanguagePollDescription) description);
        }

        poll.setTitles(titleList);
        poll.setDescriptions(descriptionList);
        poll.setCreatedBy(user);

        return pollRepository.save(poll);
    }

    @Override
    public Poll update(Poll updatedPoll) {
        return pollRepository.save(updatedPoll);
    }

    @Override
    public List<PollDBO> getPollsCreatedByUser(Long userId) {
        return applicationContext.getBean(PollService.class).getPollsCreatedByUserByLanguage(userId, DEFAULT_LANGUAGE);
    }

    @Override
    public List<PollDBO> getPollsCreatedByUserByLanguage(Long userId, String language) {
        final String lang = language == null ? DEFAULT_LANGUAGE : language;
        List<Poll> polls = pollRepository.findAll();
        User user = userService.getById(userId);

        return polls.stream()
                .filter(poll -> poll.getCreatedBy().equals(user))
                .map(poll -> mapPollToDBO(lang, poll, objectMapper))
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    @Override
    public Poll grantAccessToUsers(Long pollId, MultipartFile file) {
        Poll poll = pollRepository.getOne(pollId);

        BufferedReader bfReader = null;
        try (InputStream is = new ByteArrayInputStream(file.getBytes())) {
            bfReader = new BufferedReader(new InputStreamReader(is));
            String temp = null;
            while ((temp = bfReader.readLine()) != null) {
                poll.getAccessPollUsers().add(userService.getByUsername(temp));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return pollRepository.save(poll);
    }

    @Override
    public List<PollDBO> getAccessiblePolls(Long userId) {
        return applicationContext.getBean(PollService.class).getAccessiblePollsByLanguage(userId, DEFAULT_LANGUAGE);
    }

    @Override
    public List<PollDBO> getAccessiblePollsByLanguage(Long userId, String language) {
        final String lang = language == null ? DEFAULT_LANGUAGE : language;
        List<Poll> allPolls = pollRepository.findAll();
        User user = userService.getById(userId);

        return allPolls.stream()
                .filter(poll -> poll.getAccessPollUsers().contains(user) || poll.getCreatedBy().equals(user))
                .map(poll -> mapPollToDBO(lang, poll, objectMapper)).filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    @Override
    public Poll grantAccessToUser(Long pollId, Long userId) {
        Poll poll = pollRepository.getOne(pollId);
        User user = userService.getById(userId);

        poll.getAccessPollUsers().add(user);
        return pollRepository.save(poll);
    }

    @Override
    public PollDBO revokeAccessFromUser(Long pollId, Long userId) {
        Poll poll = pollRepository.getOne(pollId);
        User user = userService.getById(userId);

        if(poll.getAccessPollUsers().stream().filter(user1 -> user1.equals(user)).count() == 1) {
            poll.getAccessPollUsers().removeAll(Collections.singleton(user));
            pollRepository.save(poll);
        }

        return mapPollToDBO(DEFAULT_LANGUAGE, poll, objectMapper);
    }

}
