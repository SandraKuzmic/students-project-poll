package hr.fer.digedu.api.service.impl;

import hr.fer.digedu.api.model.Role;
import hr.fer.digedu.api.model.User;
import hr.fer.digedu.api.repository.RoleRepository;
import hr.fer.digedu.api.repository.UserRepository;
import hr.fer.digedu.api.service.UserService;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    public UserServiceImpl(UserRepository userRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }

    @Override
    public User getById(Long id) {
        User user = userRepository.getOne(id);
        return (User) Hibernate.unproxy(user);
    }

    @Override
    public User getByUsername(String username) {
        Optional<User> optional = userRepository.findByUsername(username);
        return optional.orElse(null);
    }

    @Override
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    public User assignUserToModerator(User assignee, User assigner) {
        Role moderatorRole = roleRepository.findByRole("ROLE_MODERATOR");
        Role userRole = roleRepository.findByRole("ROLE_USER");

        if (!assigner.getRoles().contains(moderatorRole) || !assignee.getRoles().contains(userRole)) {
            return null;
        }

        assigner.getAssignedUsers().add(assignee);

        return userRepository.save(assigner);
    }

    @Override
    public User unassignUserFromModerator(User user, User moderator) {
        if (moderator.getAssignedUsers().stream().filter(user1 -> user1.equals(user)).count() == 1) {
            moderator.getAssignedUsers().removeAll(Collections.singleton(user));
        }

        return userRepository.save(moderator);
    }

    @Override
    public Set<User> getAssignedUsersForModerator(Long moderatorId) {
        User user = userRepository.getOne(moderatorId);
        Role moderatorRole = roleRepository.findByRole("ROLE_MODERATOR");

        if (user.getRoles().contains(moderatorRole)) {
            return user.getAssignedUsers();
        }

        return Collections.emptySet();
    }

    @Override
    public List<User> getUsersByRole(String roleName) {
        Role role = roleRepository.findByRole(roleName);
        return userRepository.findByRolesContaining(role);
    }

}
