package hr.fer.digedu.api.model;

import java.util.ArrayList;
import java.util.List;

public class CsvImported<T> {

    private int successful = 0;
    private int failed = 0;
    private List<T> list;

    public CsvImported() {
        this.list = new ArrayList<>();
    }

    public void increaseFailed() {
        this.failed++;
    }

    public void addSuccessful(T object){
        list.add(object);
        this.successful++;
    }

    public int getSuccessful() {
        return successful;
    }

    public int getFailed() {
        return failed;
    }

    public List<T> getList() {
        return list;
    }
}
