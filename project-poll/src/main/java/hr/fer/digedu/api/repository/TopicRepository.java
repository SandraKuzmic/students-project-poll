package hr.fer.digedu.api.repository;

import hr.fer.digedu.api.model.User;
import hr.fer.digedu.api.model.topic.Topic;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TopicRepository extends JpaRepository<Topic, Long> {

    List<Topic> findByPollId(Long pollId);

    List<Topic> findByAssignedUsersContaining(User userId);

}
