package hr.fer.digedu.api.service;

import hr.fer.digedu.api.model.User;
import java.util.List;
import java.util.Set;

/**
 * User service is contains methods for fetching and editing user profiles in polls application. Users aren't registered
 * in our application. User data is received from OAuth2 server and only a copy of that data is stored in this application.
 * User password and sensitive data will never be persisted.
 */
public interface UserService {

    /**
     * Fetches all users from database
     *
     * @return List of {@link User} or empty list if there are none
     */
    List<User> getAll();

    /**
     * Finds single user by given id.
     *
     * @param id {@link User} id
     * @return {@link User} object if found
     * @throws javax.persistence.EntityNotFoundException if there is no user with given id.
     */
    User getById(Long id);

    /**
     * Finds single user by given username.
     *
     * @param username {@link User} id
     * @return {@link User} object if found, otherwise null
     */
    User getByUsername(String username);

    /**
     * Saves user data
     *
     * @param user user ubject we want to save
     * @return updated user object
     */
    User save(User user);

    /**
     * Assigns user to moderator.
     *
     * @param user {@link User} that we want to assign
     * @param moderator {@link User} with moderator role
     * @return updated moderator user
     */
    User assignUserToModerator(User user, User moderator);

    /**
     * Unassigns user from moderator
     *
     * @param user {@link User} that we want to unassign
     * @param moderator {@link User} with moderator role
     * @return updated moderator user
     */
    User unassignUserFromModerator(User user, User moderator);

    /**
     * Fetches assigned users for moderator
     *
     * @param moderatorId id of moderator user
     * @return Assigned user set for moderator or emtpy set if user isn't moderator
     */
    Set<User> getAssignedUsersForModerator(Long moderatorId);

    /**
     * Finds all users with assigned role
     *
     * @param roleName name of the role that we want to assign
     * @return List of users with assigned role
     */
    List<User> getUsersByRole(String roleName);

}
