package hr.fer.digedu.api.web.v1;

import hr.fer.digedu.api.model.Country;
import hr.fer.digedu.api.service.CountryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.Size;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/country")
@Api(value = "/api/v1/country", tags = {"Country"}, produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
public class CountryControllerV1 {

    private final CountryService countryService;

    public CountryControllerV1(CountryService countryService) {
        this.countryService = countryService;
    }

    @GetMapping
    @ApiOperation(value = "Finds all countries", response = Country.class, responseContainer = "List")
    public ResponseEntity<List<Country>> getAll() {
        return ResponseEntity.ok(countryService.getAll());
    }

    @GetMapping("/{code}")
    @ApiOperation(value = "Finds country with given id", response = Country.class)
    public ResponseEntity<Country> getCountryByCode(@PathVariable("code") @Valid @Size(min = 2, max = 2) String code) {
        return ResponseEntity.ok(countryService.getByCode(code.toUpperCase()));
    }

}
