package hr.fer.digedu.auth

import hr.fer.digedu.auth.service.EmailService
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner::class)
@Ignore
class MailTest {

    @Autowired
    lateinit var emailService: EmailService

    @Ignore
    fun prepareTemplate() {
        val params = mapOf("username" to "mbartolac", "url" to "http://localhost:8080/app")
        val templateName = "mail-registration-successful"

        emailService.prepareTemplate(params, templateName)
    }
}