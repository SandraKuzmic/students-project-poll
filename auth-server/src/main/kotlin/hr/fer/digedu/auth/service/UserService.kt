package hr.fer.digedu.auth.service

import hr.fer.digedu.auth.domain.User
import hr.fer.digedu.auth.web.dto.RegistrationDTO
import java.util.*

interface UserService {

    fun save(user: User): User
    fun findAll(): List<User>?
    fun delete(id: Long)
    fun findUser(username: String): User?
    fun getByResetToken(resetToken: String): User?
    fun getById(id: Long): User
    fun setResetToken(token: UUID, username: String)
    fun registerUser(registrationDTO: RegistrationDTO)
}
