package hr.fer.digedu.auth.domain

import javax.persistence.*

@Entity
@Table(name = "users")
data class User(
        @Column(unique = true) var username: String = "",
        var firstName: String = "",
        var lastName: String = "",
        var password: String = "",
        @Column(unique = true) var email: String = "",
        @OneToMany(fetch = FetchType.EAGER)
        @JoinTable(
                name = "user_roles",
                joinColumns = [JoinColumn(name = "user_id")],
                inverseJoinColumns = [JoinColumn(name = "role_id")]
        )
        var roles: List<Role> = listOf(Role("ROLE_USER")),
        @Column(nullable = true, unique = true) var resetToken: String? = null,
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long = 0
)
