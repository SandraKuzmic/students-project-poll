package hr.fer.digedu.auth.service.impl

import hr.fer.digedu.auth.service.EmailService
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.mail.javamail.MimeMessageHelper
import org.springframework.mail.javamail.MimeMessagePreparator
import org.springframework.stereotype.Service
import org.thymeleaf.TemplateEngine
import org.thymeleaf.context.Context
import javax.mail.internet.MimeMessage

@Service
class EmailServiceImpl(private val mailSender: JavaMailSender, private val templateEngine: TemplateEngine) : EmailService {

    override fun sendMail(recipient: String?, subject: String?, params: Map<String, String>, templateName: String) {
        val messagePreparator = MimeMessagePreparator { mimeMessage: MimeMessage? ->
            val messageHelper = MimeMessageHelper(mimeMessage)
            messageHelper.setFrom("projekt4696@gmail.com")
            messageHelper.setTo(recipient)
            messageHelper.setSubject(subject)
            val content: String = prepareTemplate(params, templateName)
            messageHelper.setText(content, true)
        }

        mailSender.send(messagePreparator)
    }

    override fun prepareTemplate(params: Map<String, String>, templateName: String): String {
        val context = Context()
        params.forEach { key, value ->
            context.setVariable(key, value)
        }
        return templateEngine.process(templateName, context);
    }
}