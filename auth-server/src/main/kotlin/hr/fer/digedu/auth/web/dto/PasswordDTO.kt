package hr.fer.digedu.auth.web.dto

data class PasswordDTO(
        val password: String = ""
)
