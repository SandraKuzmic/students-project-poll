package hr.fer.digedu.auth.domain

import org.springframework.security.core.authority.SimpleGrantedAuthority
import javax.persistence.*

@Entity
@Table(name = "roles")
data class Role(val role: String, @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long = 0) {

    fun getSimpleAuthority(): SimpleGrantedAuthority {
        return SimpleGrantedAuthority(this.role)
    }
}
