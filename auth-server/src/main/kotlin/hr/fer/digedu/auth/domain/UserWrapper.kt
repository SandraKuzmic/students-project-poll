package hr.fer.digedu.auth.domain

class UserWrapper {
    var id: String? = null
        private set
    var fullName: String? = null
        private set
    var username: String? = null
        private set
    var email: String? = null
        private set
    var details: Any? = null
        private set
    var authorities: Any? = null
        private set
    var name: String? = null
        private set

    fun setId(id: String?): UserWrapper {
        this.id = id
        return this
    }

    fun setFullName(fullName: String?): UserWrapper {
        this.fullName = fullName
        return this
    }

    fun setUsername(username: String?): UserWrapper {
        this.username = username
        return this
    }

    fun setEmail(email: String?): UserWrapper {
        this.email = email
        return this
    }

    fun setDetails(details: Any?): UserWrapper {
        this.details = details
        return this
    }

    fun setAuthorities(authorities: Any?): UserWrapper {
        this.authorities = authorities
        return this
    }

    fun setName(name: String?): UserWrapper {
        this.name = name
        return this
    }
}