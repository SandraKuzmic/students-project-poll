package hr.fer.digedu.auth.service

interface EmailService {

    fun sendMail(recipient: String?, subject: String?, params: Map<String, String>, templateName: String)
    fun prepareTemplate(params: Map<String, String>, templateName: String): String?
}